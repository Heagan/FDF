SDL = -I C:/SDL2/include -L C:/SDL2/lib/x64 -lSDL2main -lSDL2
LIB	= -I libft -L libft -lft

NAME = fdf

all:
	@make -C libft
	@echo -n "Compiling... "
	@gcc -o $(NAME) fdf.c $(LIB) $(SDL)
	@echo "Done"

clean:
	@rm $(NAME)

fclean: clean
	@make -C libft fclean

re: fclean all