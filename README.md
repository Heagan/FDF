# FDF

FDF is a graphics project from school written in C
It read a map file full of coordinates and then draws them onto the screen

# To Run
Make sure you have GCC, and Make installed and configured on your machine

<b>make all</b>

`./fdf <map file>`

# Preview
<b>This map file will produce the image below
<br>[MAP FILE](maps/42.fdf)
<br>![](42.png)