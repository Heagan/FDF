#include "libft/libft.h"
#include <fcntl.h>
#include <SDL.h>

int width = 1600;
int height = 900;

SDL_Window		*window;
SDL_Surface		*surface;
SDL_Event		event;

int		mkcolour(int min, int max) {
	min = min ? min : 1;
	max = max ? max : 1;

	float n = (255 * (max / min) * 256 * 256) + (255 * (min / max) * 256);

	return n;
}

void	putpixel(unsigned int x, unsigned int y, unsigned int colour) {
    unsigned int	*pxlptr;
 	int				lineoffset;

 	if ((int)y >= height || (int)y < 0)
 		return;
 	if ((int)x >= width || (int)x < 0)
 		return;
	lineoffset = (y * width) + x;
	pxlptr = (unsigned int *)surface->pixels;
	pxlptr[lineoffset] = colour;
}

void DDA(int X0, int Y0, int X1, int Y1, int Z0, int Z1) {
	X0  *= 20;
	X1  *= 20;
	Y0  *= 20;
	Y1  *= 20;

	X0  += width / 2;
	X1  += width / 2;

	int fx0 = (X0 - Y0);
	int fy0 = ((X0 + Y0) - Z0 * 5 ) / 2;
	int fx1 = (X1 - Y1);
	int fy1 = ((X1 + Y1) - Z1 * 5) / 2;

    int dx = fx1 - fx0;
    int dy = fy1 - fy0;
 
    int steps = abs(dx) > abs(dy) ? abs(dx) : abs(dy);
 
    float Xinc = dx / (float) steps;
    float Yinc = dy / (float) steps;
 
    float X = fx0;
    float Y = fy0;
    for (int i = 0; i <= steps; i++) {
        putpixel (X,Y, mkcolour(Z0, Z1));
        X += Xinc;
        Y += Yinc;
    }
}

void		loadMap(int map[1000][1000], char *file) {
	int		fd;
	char	*line;
	char	**slt;
	int		x, y = 0;

	fd = open(file, O_RDONLY);
	if (fd < 0) {
		puts("Invaild File!");
		exit(0);
	}
	while (get_next_line(fd, &line)) {
		slt = ft_strsplit(line, ' ');

		x = 0;
		while (slt[x]) {
			map[y][x] = ft_atoi(slt[x]);
			x++;
			map[y][x] = -1000;
		}
		y++;
		map[y][0] = -1000;
	}
}

void	drawMap(int map[1000][1000]) {
	int x, y = 0;

	while (map[y][0] != -1000) {
		x = 0;
		while (map[y][x] != -1000) {
			if (map[y][x + 1] != -1000)
				DDA(x, y, x + 1, y, map[y][x], map[y][x + 1]);
			if (map[y + 1][0] != -1000)
				DDA(x, y, x, y + 1, map[y][x], map[y + 1][x]);
			x++;
		}
		y++;
	}
}

int main(int ac, char **av) {
	static int	map[1000][1000];

	if (ac == 1) {
		puts("Usage: ./fdf <map>");
		return 1;
	}

	SDL_Init(SDL_INIT_EVERYTHING);
	window = SDL_CreateWindow("FDF - SDL", 0, 0, width, height, SDL_WINDOW_SHOWN);
	surface = SDL_GetWindowSurface(window);

	loadMap(map, av[1]);
	drawMap(map);

	while (1) {
		if (SDL_PollEvent(&event))
			if (event.type == SDL_QUIT) {
				break ;
			}
		SDL_UpdateWindowSurface(window);
	}
}